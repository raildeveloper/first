class User < ApplicationRecord
	has_many :articles
  # Include default devise modules. Others available are:
  # , :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable

  has_attached_file :avtar, styles:{medium: "300x300", thumb:"100x100"}
  validates_attachment_content_type :avtar, content_type: /\Aimage\/.*\Z/
end
